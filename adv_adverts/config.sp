#if defined __adv_adverts_config
 #endinput
#endif
#define __adv_adverts_config

stock Config_Init()
{
	g_hAdvertsArray = CreateArray();
}

stock Config_SetupFile()
{
	Config_DeleteExistingHandles();
	new bool:preConfig;
	if ((preConfig = Forwards_CallPreConfig()))
	{
		new Handle:kv = CreateKeyValues("Advertisements");
		if (FileToKeyValues(kv, g_strConfigPath))
		{
			if (KvGotoFirstSubKey(kv))
			{
				decl String:keyName[KEY_MAX_LENGTH];
				do
				{
					new Handle:array = CreateArray();
					KvGetSectionName(kv, keyName, sizeof(keyName));
					PushArrayString(g_hAdvertsArray, keyName);
					if (KvGotoFirstSubKey(kv, false))
					{
						decl String:keyVal[KEYVAL_MAX_LENGTH];
						do
						{
							new Handle:array2 = CreateArray();
							KvGetSectionName(kv, keyName, sizeof(keyName));
							PushArrayString(array2, keyName);
							KvGetString(kv, NULL_STRING, keyVal, sizeof(keyVal));
							PushArrayString(array2, keyVal);
							PushArrayCell(array2, _:KvGetDataType(kv, NULL_STRING));

							PushArrayCell(array, _:array2);
						}
						while (KvGotoNextKey(kv, false));
					}
					SetTrieValue(g_hAdvertsTrie, keyName, _:array);
				}
				while (KvGotoNextKey(kv));
			}
		}
		else
		{
			SetFailState("Could not load config file at %s", g_strConfigPath);
		}
		CloseHandle(kv);
	}
	Forwards_CallPostConfig(preConfig);
}

stock Config_DeleteExistingHandles()
{
	if (g_hAdvertsArray != INVALID_HANDLE)
	{
		decl String:keyName[KEY_MAX_LENGTH];
		new size = GetArraySize(g_hAdvertsArray);
		for (new i = 0; i < size; i++)
		{
			GetArrayString(g_hAdvertsArray, i, keyName, sizeof(keyName));
			new Handle:array = INVALID_HANDLE;
			if (GetTrieValue(g_hAdvertsTrie, keyName, _:array) && array != INVALID_HANDLE)
			{
				CloseHandle(array);
			}
		}
	}
	CloseHandle(g_hAdvertsTrie);
	CloseHandle(g_hAdvertsArray);
}