#if defined __adv_adverts_autoupdate
 #endinput
#endif
#define __adv_adverts_autoupdate

stock AutoUpdate_Setup()
{
	new Handle:convar;
	if (LibraryExists("updater")) 
	{
		Updater_AddPlugin(UPDATE_URL);
		new String:newVersion[10];
		FormatEx(newVersion, sizeof(newVersion), "%sA", PLUGIN_VERSION);
		convar = CreateConVar("adverts_version", newVersion, "backpack.tf Price Check Version", FCVAR_DONTRECORD|FCVAR_NOTIFY|FCVAR_CHEAT);
	}
	else 
	{
		convar = CreateConVar("adverts_version", PLUGIN_VERSION, "backpack.tf Price Check Version", FCVAR_DONTRECORD|FCVAR_NOTIFY|FCVAR_CHEAT);	
	}
	HookConVarChange(convar, Callback_VersionConVarChanged);
}

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater")) 
	{
		Updater_AddPlugin(UPDATE_URL);
	}
}

public Callback_VersionConVarChanged(Handle:conVar, const String:oldValue[], const String:newValue[]) 
{
	ResetConVar(conVar);
}

public Action:Updater_OnPluginDownloading() 
{
	if (!g_bAutoUpdate) 
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Updater_OnPluginUpdated() 
{
	ReloadPlugin();
}