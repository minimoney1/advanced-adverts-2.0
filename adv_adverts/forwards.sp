#if defined __adv_adverts_forwards
 #endinput
#endif
#define __adv_adverts_forwards

static Handle:g_hPreConfigFwd = INVALID_HANDLE,
	   Handle:g_hPostConfigFwd = INVALID_HANDLE,
	   Handle:g_hPreTimerFwd = INVALID_HANDLE,
	   Handle:g_hTimerFwd = INVALID_HANDLE,
	   Handle:g_hPostTimerFwd = INVALID_HANDLE; 

stock Forwards_Register()
{
	g_hPreConfigFwd = CreateForward(ET_Hook);
	g_hPostConfigFwd = CreateForward(ET_Ignore, Param_Cell);
	g_hPreTimerFwd = CreateForward(ET_Hook, Param_Cell, Param_String);
	g_hTimerFwd = CreateForward(ET_Hook, Param_Cell, Param_String);
	g_hPostTimerFwd = CreateForward(ET_Hook, Param_Cell, Param_String, Param_Cell);
}

stock bool:Forwards_AddPostTimer(Handle:plugin, Function:func)
{
	return AddToForward(g_hPostTimerFwd, plugin, func);
}

stock bool:Forwards_RemovePostTimer(Handle:plugin, Function:func)
{
	return RemoveFromForward(g_hPostTimerFwd, plugin, func);
}

stock Forwards_CallPostTimer(client, String:key[], bool:postTimer = true)
{
	Call_StartForward(g_hPostTimerFwd);
	Call_PushCell(client);
	Call_PushString(key);
	Call_PushCell(postTimer);
	Call_Finish();
}

stock bool:Forwards_AddTimer(Handle:plugin, Function:func)
{
	return AddToForward(g_hTimerFwd, plugin, func);
}

stock bool:Forwards_RemoveTimer(Handle:plugin, Function:func)
{
	return RemoveFromForward(g_hTimerFwd, plugin, func);
}

stock bool:Forwards_CallTimer(client, String:key[])
{
	new Action:ret = Plugin_Continue;
	Call_StartForward(g_hTimerFwd);
	Call_PushCell(client);
	Call_PushString(key);
	Call_Finish(ret);

	return (ret == Plugin_Handled || ret == Plugin_Stop);
}

stock bool:Forwards_AddPreTimer(Handle:plugin, Function:func)
{
	return AddToForward(g_hPreTimerFwd, plugin, func);
}

stock bool:Forwards_RemovePreTimer(Handle:plugin, Function:func)
{
	return RemoveFromForward(g_hPreTimerFwd, plugin, func);
}

stock bool:Forwards_CallPreTimer(client, String:key[])
{
	new Action:ret = Plugin_Continue;
	Call_StartForward(g_hPreTimerFwd);
	Call_PushCell(client);
	Call_PushStringEx(key, KEY_MAX_LENGTH, SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
	Call_Finish(ret);

	return (ret == Plugin_Handled || ret == Plugin_Stop);
}

stock bool:Forwards_AddPostConfig(Handle:plugin, Function:func)
{
	return AddToForward(g_hPostConfigFwd, plugin, func);
}

stock bool:Forwards_RemovePostConfig(Handle:plugin, Function:func)
{
	return RemoveFromForward(g_hPostConfigFwd, plugin, func);
}

stock Forwards_CallPostConfig(bool:preConfig = true)
{
	Call_StartForward(g_hPostConfigFwd);
	Call_PushCell(preConfig);
	Call_Finish();
}

stock bool:Forwards_AddPreConfig(Handle:plugin, Function:func)
{
	return AddToForward(g_hPreConfigFwd, plugin, func);
}

stock bool:Forwards_RemovePreConfig(Handle:plugin, Function:func)
{
	return RemoveFromForward(g_hPreConfigFwd, plugin, func);
}


stock bool:Forwards_CallPreConfig()
{
	new Action:ret = Plugin_Continue;
	Call_StartForward(g_hPreConfigFwd);
	Call_Finish(ret);

	return (ret == Plugin_Handled || ret == Plugin_Stop);
}