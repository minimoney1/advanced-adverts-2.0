#if defined __adv_adverts_commands
 #endinput
#endif
#define __adv_adverts_commands

stock Commands_Register()
{
	RegAdminCmd("sm_reloadads", Command_ReloadAds, ADMFLAG_CONFIG);
	RegAdminCmd("sm_shownextad", Command_ShowNextAd, ADMFLAG_GENERIC);
}

public Action:Command_ReloadAds(client, args)
{
	Config_SetupFile();
	ReplyToCommand(client, "[SM] %t", "Reloaded_Config");
	return Plugin_Handled;
}

public Action:Command_ShowNextAd(client, args)
{
	if (args > 0)
	{
		decl String:arg[1024];
		GetCmdArgString(arg, sizeof(arg));
		new target = FindTarget(client, arg);
		if (target && IsClientInGame(target))
		{
			Timer_ShowClientAdvert(target);
		}
		ReplyToCommand(client, "[SM] %t", "Showed_Advert");
		return Plugin_Handled;
	}
	for (new x = 1; x <= MaxClients; x++)
	{
		if (IsClientInGame(x) && CanUserTarget(client, x))
		{
			Timer_ShowClientAdvert(x);
		}
	}
	ReplyToCommand(client, "[SM] %t", "Showed_Advert_All");
	return Plugin_Handled;
}