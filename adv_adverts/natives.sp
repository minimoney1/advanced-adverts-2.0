#if defined __adv_adverts_natives
 #endinput
#endif
#define __adv_adverts_natives

stock Natives_Register()
{
	CreateNative("AdvertHook", Native_AdvertHook);
	CreateNative("AdvertUnhook", Native_AdvertUnhook);
	CreateNative("GetAdvertCount", Native_GetAdvertCount);
	CreateNative("GetAdvert", Native_GetAdvert);
}

public Native_GetAdvert(Handle:plugin, numParams)
{
	new index = GetNativeCell(1);
	if (index < GetArraySize(g_hAdvertsArray))
	{
		new size = GetNativeCell(3);
		decl String:key[size];
		GetArrayString(g_hAdvertsArray, index, key, size);
		return (SetNativeString(2, key, size) == SP_ERROR_NONE);
	}
	return false;
}

public Native_GetAdvertCount(Handle:plugin, numParams)
{
	return GetArraySize(g_hAdvertsArray);
}

public Native_AdvertHook(Handle:plugin, numParams)
{
	new AdvertHookType:type = AdvertHookType:GetNativeCell(1);
	switch (type)
	{
		case AdvertHook_PreConfig:
		{
			return Forwards_AddPreConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PostConfig:
		{
			return Forwards_AddPostConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PreTimer:
		{
			return Forwards_AddPostTimer(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_Timer:
		{
			return Forwards_AddTimer(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PostTimer:
		{
			return Forwards_AddPreTimer(plugin, Function:GetNativeCell(2));
		}
	}
	return false;
}

public Native_AdvertUnhook(Handle:plugin, numParams)
{
	new AdvertHookType:type = AdvertHookType:GetNativeCell(1);
	switch (type)
	{
		case AdvertHook_PreConfig:
		{
			return Forwards_RemovePreConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PostConfig:
		{
			return Forwards_RemovePostConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PreTimer:
		{
			return Forwards_RemovePostConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_Timer:
		{
			return Forwards_RemovePostConfig(plugin, Function:GetNativeCell(2));
		}
		case AdvertHook_PostTimer:
		{
			return Forwards_RemovePostConfig(plugin, Function:GetNativeCell(2));
		}
	}
	return false;
}