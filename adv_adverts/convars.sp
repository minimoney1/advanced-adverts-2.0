#if defined __adv_adverts_convars
 #endinput
#endif
#define __adv_adverts_convars

stock ConVars_Register()
{
	new Handle:conVar;
	conVar = CreateConVar("adverts_enable", "1", "Enable this plugin.");
	g_bEnabled = GetConVarBool(conVar);
	HookConVarChange(conVar, OnEnableChanged);

	conVar = CreateConVar("adverts_autoupdate", "1", "Should this plugin auto-update?");
	g_bAutoUpdate = GetConVarBool(conVar);
	HookConVarChange(conVar, OnAutoUpdateChange);

	conVar = CreateConVar("adverts_interval", "10.0", "How long should the interval between adverts be?");
	for (new i = 1; i <= MAXPLAYERS; i++)
	{
		g_fInterval[i] = GetConVarFloat(conVar);
	}
	HookConVarChange(conVar, OnIntervalChange);

	conVar = CreateConVar("adverts_path", "configs/adv_advets.txt", "The path to the adverts config folder, relative to the SM base folder.");
	GetConVarString(conVar, g_strConfigPath, sizeof(g_strConfigPath));
	BuildPath(Path_SM, g_strConfigPath, sizeof(g_strConfigPath), g_strConfigPath);
	HookConVarChange(conVar, OnPathChange);

	AutoExecConfig();
}

public OnEnableChanged(Handle:conVar, const String:oldVal[], const String:newVal[])
{
	g_bEnabled = bool:StringToInt(newVal);
}

public OnAutoUpdateChange(Handle:conVar, const String:oldVal[], const String:newVal[])
{
	g_bAutoUpdate = bool:StringToInt(newVal);
}

public OnIntervalChange(Handle:conVar, const String:oldVal[], const String:newVal[])
{
	new Float:buffer = StringToFloat(newVal);
	for (new i = 0; i <= MaxClients; i++)
	{
		g_fInterval[i] = buffer;
	}
}

public OnPathChange(Handle:conVar, const String:oldVal[], const String:newVal[])
{
	BuildPath(Path_SM, g_strConfigPath, sizeof(g_strConfigPath), newVal);
}