#if defined __adv_adverts_globals
 #endinput
#endif
#define __adv_adverts_globals

#define UPDATE_URL ""
#define PLUGIN_VERSION "2.0.0-prealpha"
#define KEY_MAX_LENGTH 256
#define KEYVAL_MAX_LENGTH 1024

enum ArrayIndex
{
	ArrayIndex_Key,
	ArrayIndex_KeyVal,
	ArrayIndex_KVType,
};

new g_iCurrentArrayPos[MAXPLAYERS + 1] = 0;

new bool:g_bLate,
	bool:g_bEnabled,
	bool:g_bAutoUpdate;
new Float:g_fInterval[MAXPLAYERS + 1];

new String:g_strConfigPath[PLATFORM_MAX_PATH];

new String:g_strNextKey[KEY_MAX_LENGTH][MAXPLAYERS + 1];

new Handle:g_hAdvertsArray = INVALID_HANDLE,
	Handle:g_hAdvertsTrie = INVALID_HANDLE,
	Handle:g_hAdvertTimer[MAXPLAYERS + 1] = {INVALID_HANDLE, ...};