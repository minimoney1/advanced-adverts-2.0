#if defined __adv_adverts_timer
 #endinput
#endif
#define __adv_adverts_timer

static bool:g_bSkipClient[MAXPLAYERS + 1] = {false, ...};

stock Timer_Setup(bool:autoClose = true)
{
	for (new x = 1; x <= MaxClients; x++)
	{
		if (g_hAdvertTimer[x] != INVALID_HANDLE)
		{
			if (!autoClose)
				continue;
			KillTimer(g_hAdvertTimer[x]);
		}
		if (IsClientInGame(x))
		{
			Timer_HookClient(x);
		}
		g_iCurrentArrayPos[x] = 0;
	}
}

stock Timer_ShowClientAdvert(client)
{
	Timer_AdvertTimer(g_hAdvertTimer[client], GetClientUserId(client));
}

stock Timer_HookClient(client, bool:dontStart = false)
{
	g_hAdvertTimer[client] = CreateTimer(g_fInterval[client], Timer_AdvertTimer, GetClientUserId(client), TIMER_REPEAT);
	g_bSkipClient[client] = dontStart;
}

stock Timer_StartClientHook(client)
{
	if (g_bSkipClient[client])
	{
		g_bSkipClient[client] = false;
		Timer_ShowClientAdvert(client);
	}
}

stock Timer_PauseClientHook(client)
{
	if (!g_bSkipClient[client])
	{
		g_bSkipClient[client] = true;
	}
}

stock Timer_UnhookClient(client)
{
	if (g_hAdvertTimer[client] == INVALID_HANDLE)
	{
		KillTimer(g_hAdvertTimer[client]);
	}
}

public Action:Timer_AdvertTimer(Handle:timer, any:userid)
{
	new client = GetClientOfUserId(userid);
	if (client > 0 && IsClientInGame(client))
	{
		if (!g_bSkipClient[client])
		{
			Call_StartFunction(INVALID_HANDLE, OnAdvertTimer);
			Call_PushCell(client);
			Call_Finish();
		}
		
	}
	else
	{
		g_hAdvertTimer[client] = INVALID_HANDLE;
		return Plugin_Stop;
	}
	return Plugin_Continue;
}