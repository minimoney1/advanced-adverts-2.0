#if defined __adv_adverts_events
 #endinput
#endif
#define __adv_adverts_events

#include <sourcemod>

stock Events_Register()
{
	HookEvent("player_connect", Event_PlayerConnect);
	HookEvent("player_activate", Event_PlayerActivate);
	HookEvent("player_disconnect", Event_PlayerDisconnect);
}

public Action:Event_PlayerConnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client > 0 && IsClientInGame(client))
	{
		Timer_HookClient(client, true);
	}
}

public Action:Event_PlayerActivate(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client > 0 && IsClientInGame(client))
	{
		Timer_StartClientHook(client);
	}
}

public Action:Event_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (client > 0 && IsClientInGame(client))
	{
		Timer_UnhookClient(client);
	}
}