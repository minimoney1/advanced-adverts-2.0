#include <sourcemod>
#include <updater>
#include <adv_adverts>

#include "adv_adverts/globals.sp"
#include "adv_adverts/forwards.sp"
#include "adv_adverts/config.sp"
#include "adv_adverts/timer.sp"
#include "adv_adverts/events.sp"
#include "adv_adverts/commands.sp"
#include "adv_adverts/natives.sp"
#include "adv_adverts/convars.sp"
#include "adv_adverts/autoupdate.sp"

public APLRes:AskPluginLoad2(Handle:plugin, bool:late, String:error[], err_max)
{
	g_bLate = late;
	Natives_Register();
	return APLRes_Success;
}

public OnPluginStart()
{
	AutoUpdate_Setup();
	Forwards_Register();
	ConVars_Register();
	Config_Init();
	Events_Register();

	LoadTranslations("adv_adverts.phrases");
}

public OnConfigsExecuted()
{
	Config_SetupFile();
}

public OnMapStart()
{
	Timer_Setup();
}

public OnAdvertTimer(client)
{
	if (g_bEnabled)
	{
		new bool:operate = Forwards_CallPreTimer(client, g_strNextKey[client]);
		if (operate)
		{
			Forwards_CallTimer(client, g_strNextKey[client]);
		}
		Forwards_CallPostTimer(client, g_strNextKey[client], operate);
	}
}