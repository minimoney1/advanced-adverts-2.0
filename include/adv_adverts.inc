#if defined __scp_included_
 #endinput
#endif
#define __scp_included_

enum AdvertHookType
{
	AdvertHook_PreConfig,
	AdvertHook_PostConfig,
	AdvertHook_PreTimer,
	AdvertHook_Timer,
	AdvertHook_PostTimer,
};

/*
 *
 *
 *
 *
 *
 *
 *
 */
native AdvertHook(AdvertHookType:type, AdvertHookCB:func);
/*
 *
 *
 *
 *
 *
 *
 *
 */
native AdvertUnhook(AdvertHookType:type, AdvertHookCB:func);